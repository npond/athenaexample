# Athena Example
The code in this repository is designed to mimic the skeleton of some analysis code that uses Athena, for use at the ATLAS UK software tutorial.

## Initial setup
Setup and compile the code (note these steps only need to be done once).
```
setupATLAS
cd athenaexample 
mkdir build run
cd build 
asetup AthAnalysis,22.2.100
cmake ../source
source $TestArea/*/setup.sh
cmake --build $TestArea
```
If you logout, and then login again in a fresh session, then all you need to do is
```
setupATLAS
lsetup cmake
cd build
asetup
source $TestArea/*/setup.sh
cd ..
```
If you need to recompile at any point, you need only  do:
```
cd build
cmake --build $TestArea
```
Unless you add more packages, in which case you will need to do `cmake ../source` again. 

## How to run the code
To run the code, do 
```
athena NtupleMaker/NtupleMakerAlgJobOptions.py --filesInput=$ASG_TEST_FILE_MC --evtMax=10
```

Alternatively you can run the code using the new `ComponentAccumulator` based script

```
python3 -m NtupleMaker.run --filesInput=$ASG_TEST_FILE_MC --evtMax=10 
```

## Example results
If you're lucky, you might see a nice picture of a cow. 
This means the code is working correctly.
You can find the code for where this is implemented in ntupleMakerAlg.cxx .
